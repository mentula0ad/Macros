var g_Macros;

init = new Proxy(init, {apply: function(target, thisArg, args) {
    target(...args);
    g_Macros = new Macros();
}});
