for (const type in g_UnitActions)
{
    const execute = g_UnitActions[type].execute;
    g_UnitActions[type].execute = function(...args)
    {
        g_Macros.record(type, args);
        return execute(...args);
    }
}
