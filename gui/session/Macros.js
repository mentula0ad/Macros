class Macros
{

    constructor()
    {
        this.commands = [];
        this.recording = false;
    }

    start()
    {
        if (this.recording)
            return;

        this.commands = [];
        this.recording = true;

        this.notify("Macro recording started.");
    }

    stop()
    {
        if (!this.recording)
            return;

        this.recording = false;

        this.notify("Macro recording stopped.");
    }

    toggle()
    {
        (this.recording) ? this.stop() : this.start();
    }

    record(type, args)
    {
        if (!this.recording)
            return;

        this.commands.push([type, args]);
    }

    run()
    {
        if (this.recording)
            return;

        for (const command of this.commands)
        {
            const type = command[0];
            const args = command[1];
            g_UnitActions[type].execute(...args);
        }

        this.notify("Running recorded macro.");
    }

    notify(text)
    {
        g_NetMessageTypes["macros"]({"text": text});
    }
}
