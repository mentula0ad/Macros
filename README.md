# About Macros

*Macros* is a [0 A.D.](https://play0ad.com) mod that allows recording a sequence of actions and execute it at a later stage.

**WARNING:** this mod executes automated actions. This can be considered as a cheat. When you play a game with this mod, make sure other players are aware you are using this mod and agree.

# Usage

- Start/Stop recording actions with a hotkey.
- Execute recorded actions with a hotkey.

# Installation

[Click here](https://gitlab.com/mentula0ad/Macros/-/releases/permalink/latest/downloads/macros.pyromod) to download the latest release. Install following the official 0 A.D. guide: [How to install mods?](https://trac.wildfiregames.com/wiki/Modding_Guide#Howtoinstallmods)

_Alternative downloads:_ [Latest Release (.pyromod)](https://gitlab.com/mentula0ad/Macros/-/releases/permalink/latest/downloads/macros.pyromod) | [Latest Release (.zip)](https://gitlab.com/mentula0ad/Macros/-/releases/permalink/latest/downloads/macros.zip) | [Older Releases](https://gitlab.com/mentula0ad/Macros/-/releases)

# Questions & feedback

For more information, questions and feedback, visit the [thread on the 0 A.D. forum](https://wildfiregames.com/forum/topic/107453-macros-a-pilot-mod-to-test-macros-potential/).
